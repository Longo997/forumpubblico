const mongoose = require('mongoose')

const torneiSchema = mongoose.Schema({ 
    titolo: String,
    descrizione: String,
    tipologia: String,
    data: String,
    numPosti: Number
    })
    
const Tornei = mongoose.model('Tornei', torneiSchema) 
module.exports = Tornei