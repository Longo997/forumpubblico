const mongoose = require('mongoose')

const squadreSchema = mongoose.Schema({ 
    nome: String,
    capitano: String,
    idTorneo: String
    })
    
const Squadre = mongoose.model('Squadre', squadreSchema) 
module.exports = Squadre