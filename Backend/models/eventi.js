const mongoose = require('mongoose')

const eventiSchema = mongoose.Schema({ 
    titolo: String,
    descrizione: String,
    data: String,
    numPosti: Number,
    imgPath: String
    })
    
const Eventi = mongoose.model('Eventi', eventiSchema) 
module.exports = Eventi