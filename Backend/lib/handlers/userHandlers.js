const db = require('../../db.js')
const User = require('../../models/user.js')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Admins = require('../../models/admins.js')

exports.register = async (req, res) => {
    try {
        const { firstName, lastName, email, password } = req.body

        // Validate user input
        if (!(email && password && firstName && lastName)) {
            res.status(400).send("All input is required");
        }

        // check if user already exist
        // Validate if user exist in our database
        const oldUser = await User.findOne({ email });

        if (oldUser) {
            return res.status(409).send("User Already Exist. Please Login");
        }
        //Encrypt user password
        encryptedPassword = await bcrypt.hash(password, 10);
        let role = "User"
        const isAdmin = await Admins.findOne({ email: `${email}` })

        if (isAdmin) {
            role = "Admin"
        }


        // Create user in our database
        const user = await User.create({
            firstName,
            lastName,
            email: email.toLowerCase(), // sanitize: convert email to lowercase
            password: encryptedPassword,
            role: `${role}`
        });

        // Create token
        const token = jwt.sign(
            { user_id: user._id, email, role },
            process.env.JWT_KEY,
            {
                expiresIn: "2h",
            }
        );
        // save user token
        user.token = token;
        // return new user
        res.status(201).json(user);
    } catch (err) {
        console.log(err);
    }
}


exports.login = async (req, res) => {
    try {
        const { email, password } = req.body

        if (!(email && password)) {
            res.status(400).send("All input is required");
        }

        const user = await User.findOne({ email });
        const role = user.role

        if (user && (await bcrypt.compare(password, user.password))) {
            const token = jwt.sign(
                { user_id: user._id, email, role },
                process.env.JWT_KEY,
                {
                    expiresIn: "2h",
                }
            );
            user.token = token;

            res.status(200).json(user);
        }
        res.status(400).send("Invalid Credentials");
    } catch (err) {
        console.log(err);
    }
}
