const db = require('../../db.js')

exports.createTornei = async (req, res) => {
    const { titolo, descrizione, tipologia, data, numPosti } = req.body
    await db.createTornei(titolo, descrizione, tipologia, data, numPosti)
    return res.json({ result: 'success' })
}

exports.getTornei = async (req, res) => {
    const tornei = await db.getTornei()
    return res.json(tornei)
}

exports.deleteTorneo = async (req, res) => {
    const { id } = req.body
    await db.deleteTorneo(id).then((torneo) => {
        if (!torneo) {
            return res.status(404).send();
        }
        res.send(torneo);
    }).catch((error) => {
        res.status(500).send(error);
    })

}