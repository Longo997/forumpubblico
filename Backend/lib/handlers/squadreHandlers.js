const db = require('../../db.js')

exports.createSquadre = async (req, res) => {
    const { nome, capitano, idTorneo } = req.body
    await db.createSquadre(nome, capitano, idTorneo)
    return res.json({ result: 'success' })
}

exports.getSquadre = async (req, res) => {
    const torneo = req.query.idTorneo
    const squadre = await db.getSquadre(torneo)
    return res.json(squadre)
}
