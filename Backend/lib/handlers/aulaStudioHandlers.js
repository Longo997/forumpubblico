const db = require('../../db.js')

exports.createAula = async (req, res) => {
    const { id, posti } = req.body
    await db.createAula(id, posti)
    return res.json({ result: 'success' })
}

exports.getAula = async (req, res) => {
    const aule = await db.getAula()
    return res.json(aule)
}