const db = require('../../db.js')

exports.createIscrizione = async (req, res) => {
    const { id_torneo, nomeSquadra } = req.body
    await db.createIscrizioni(id_torneo, nomeSquadra, req.user, res)
}

exports.getIscrizioniSuTorneo = async (req, res) => {
    const torneo = req.query.idTorneo   //nell'header di postman bisogna mettere idTorneo
    const iscrizioni = await db.getIscrizioniSuTorneo(torneo)
    return res.json(iscrizioni)
}

exports.deleteIscrizione = async (req, res) => {
    const { id } = req.body
    await db.deleteIscrizione(id).then((iscrizione) => {
        if (!iscrizione) {
            return res.status(404).send();
        }
        res.send(iscrizione);
    }).catch((error) => {
        res.status(500).send(error);
    })

}
exports.getIscrizioniSuUtente = async (req,res) => {
    const iscrizioni = await db.getIscrizioniSuUtente(req.user)
    return res.json(iscrizioni)
}
