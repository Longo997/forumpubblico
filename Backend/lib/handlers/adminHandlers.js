const db = require('../../db.js')

exports.createAdmin = async (req, res) => {
    const { email } = req.body
    await db.createAdmin(email)
    return res.json({ result: 'success' })
}

exports.getAdmin = async (req, res) => {
    const admin = await db.getAdmin()
    return res.json(admin)
}