const express = require('express')
const bodyParser = require('body-parser')
const routes = require('./lib/routes')
const cors = require('cors')
require("dotenv").config();

const app = express()
app.disable('x-powered-by')

const port = process.env.port || 3001

app.use('/api',cors())
app.use(bodyParser.json())
app.use('/lib/uploads', express.static('./lib/uploads'));
routes.addRoutes(app)


if(require.main === module) { 
    app.listen(port, () => {
    console.log( `Express started on http://localhost:${port}` +
      ';\nPress Ctrl-C to terminate.' )
    })
  } else {
  module.exports = app
  }
