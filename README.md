# Web And Real Time Communication Systems
In questa repository sono presenti sia il backend che il fronted della web app per il forum.

Il backend è stato realizzato tramite **Node.js** ed **Express**, mentre il frontend tramite **React**.
