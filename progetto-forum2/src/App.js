import './App.css';

//collegamenti
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Events from "./components/eventi/events.js";
import Home from './components/home.js';
import Aulastudio from './components/aulastudio.js';
import Layout from './components/layout.js';
import Competizioni from './components/tornei/competizioni.js';
import Info from './components/eventi/info';
import Dettagli from './components/tornei/dettagli';
import Iscrizione from './components/tornei/iscrizione';
import CreaTorneo from './components/tornei/creazione';
import RimuoviTorneo from './components/tornei/rimozione';
import CreaEvento from './components/eventi/creazione';
import RimuoviEvento from './components/eventi/rimozione';
import Login from './components/login/login';
import SignUp from './components/login/SignUp';
import Account from './components/login/account'

//se cambi i parametri devi cambiare anche in torneo e dettagli

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Home />} />
            <Route path="eventi" element={<Events />} />
            <Route path="Login" element={<Login />} />
            <Route path="SignUp" element={<SignUp />} />
            <Route path="competizioni" element={<Competizioni />} />
            <Route path="aulastudio" element={<Aulastudio />} />
            <Route path="info" element={<Info />} />
            <Route path="dettagli" element={<Dettagli />} />
            <Route path="iscrizione/:id" element={<Iscrizione />} />
            <Route path="creazione-torneo" element={<CreaTorneo />} />
            <Route path="rimozione-torneo" element={<RimuoviTorneo />} />
            <Route path="creazione-evento" element={<CreaEvento />} />
            <Route path="rimozione-evento" element={<RimuoviEvento />} />
            <Route path="account" element={<Account />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

//<Route path="/conf-rimozione-evento/:id" element={<ConfermaRimozioneEvento />} />
//<Route path="/conf-rimozione-torneo/:id" element={<ConfermaRimozioneTorneo />} />

export default App