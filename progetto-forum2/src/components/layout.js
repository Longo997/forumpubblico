import Navbar from "react-bootstrap/Navbar";
import Button from 'react-bootstrap/Button'
import './layout.css';
import { Outlet, Link, useNavigate } from "react-router-dom";
import {RiLogoutBoxRLine} from 'react-icons/ri'
import {RiAccountCircleFill} from 'react-icons/ri'
import { logout } from "../database/api/api";
import { React, useState } from "react"



const Layout = () => {

  let navigate = useNavigate();
  const [autenticato, setAutenticato] = useState((sessionStorage.getItem('SavedToken')) != null)

  const handleExit = () =>{
    if (!autenticato) {
      alert("Effettuare il login!")
    }else{
      logout();
      alert("Logout effettuato correttamente!")
      window.location.reload(false);
    }
  }



      return(
      <>
        <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark">
          <Navbar.Brand></Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Link to="/"><Button variant="primary">Home</Button></Link>
            <Link to="/eventi"><Button variant="primary">Eventi</Button></Link>
            <Link to="/aulastudio"><Button variant="primary">Aula Studio</Button></Link>
            <Link to="/competizioni"><Button variant="primary">Tornei</Button></Link>
          </Navbar.Collapse>
          <div className="icons">
          <Button><Link to="/account"><RiAccountCircleFill size={25} color="white"/></Link></Button>
          <Button onClick={handleExit}><RiLogoutBoxRLine size={25} color="white"/></Button>
          </div>
        </Navbar>
        <Outlet />
      </>
      )
};

export default Layout;