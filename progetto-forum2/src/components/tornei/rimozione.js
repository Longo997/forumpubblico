import React, {useEffect, useState} from 'react'
import { Link, useNavigate } from "react-router-dom";
import Card from 'react-bootstrap/Card';
import { getTornei , deleteTorneo} from '../../database/api/api';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2'

const RimuoviTorneo = () => {
  const [tornei, setTornei] = useState([])

  let navigate = useNavigate();


  const doDelete = async (id) => {
    deleteTorneo(id)
  }

  useEffect(()=>{
    getTornei().then(response => setTornei(response.data))
  }, [])
  
  function handleButtonClick (id) {

		Swal.fire({
			title: 'Confermi eliminazione?',
			//text: "Clicca sotto",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Conferma!'
		}).then((result) => {
			if (result.isConfirmed) {
				doDelete(id)
					.then(() => {
						Swal.fire(
							'Rimozione effettuata con successo!'
						);
						//navigate('/eventi');
					}
					)
					.catch(() => {
						Swal.fire(
							'Impossibile effettuare la rimozione!'
						);
					});
			}
		});
	}

  return (
    <div>
      <h2>Seleziona torneo da eliminare:</h2>
      {tornei.map(post=>(
        <Card>
          <Card.Body>
            <Card.Title>{post.titolo}</Card.Title>
            <Card.Text>{post.data}</Card.Text>
            <Button variant="danger" onClick={() => handleButtonClick(post._id)}>Seleziona</Button>
          </Card.Body>
      </Card>
      ))}
    </div>
  )
}

//<Link to={"/conf-rimozione-torneo/"+post.id}><Button variant="danger">Seleziona</Button></Link>

export default RimuoviTorneo