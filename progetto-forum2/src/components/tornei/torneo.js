import React from 'react'
import { useNavigate } from "react-router-dom";
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import './tornei.css'

const Torneo = (props) => {
  
    let navigate = useNavigate();

    const handleDettagli = () => {
        navigate("/dettagli", { state: {
            id: props.id, titolo: props.titolo, descrizione: props.descrizione, data: props.data, numPosti: props.numPosti}
        });
    }

    const handleIscrizione = () => {
        navigate(`/iscrizione/${props.id}`, { state: {
            id: props.id, titolo: props.titolo, descrizione: props.descrizione, data: props.data, numPosti: props.numPosti}
        });
    }

    return (
    <Card style={{marginRight:'auto',marginLeft:'auto',  textAlign: 'center', padding: '2%', marginTop:'2cm'}}>
        <Card.Body>
            <Card.Title> <h2>{props.titolo}</h2></Card.Title>
            <Card.Subtitle>Data: {props.data}, Posti disponibili: {props.numPosti}</Card.Subtitle>
            <Button variant="primary" style={{marginTop:'1cm', marginRight:'1cm'}} onClick={handleDettagli}>Dettagli</Button>
            <Button variant="success" style={{marginTop:'1cm'}} onClick={handleIscrizione}>Iscrizione</Button>
        </Card.Body>
    </Card>
  )
}

//<Link to={"/dettagli/"+ props.id}><Button variant="primary">Dettagli</Button></Link>
//<Link to={"/iscrizione/"+ props.id}><Button variant="success">Iscrizione</Button></Link>

export default Torneo