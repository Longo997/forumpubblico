import React, {useState} from 'react'
import { useNavigate } from "react-router-dom";
import Swal from 'sweetalert2';
import { createTorneo } from '../../database/api/api';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

const CreaTorneo = () => {
  
  const [titolo, setTitolo] = useState('')
  const [descrizione, setDescrizione] = useState('')
  const [tipologia, setTipologia] = useState('')
  const [data, setData] = useState('')
  const [numPosti, setNumPosti] = useState('')

  let navigate = useNavigate();

  const postTorneo = (e) => {
    e.preventDefault()
    if(titolo==="" || descrizione==="" || tipologia==="" || data==="" || numPosti===""){
      alert("Inserire tutti i campi richiesti");
    }else{
      Swal.fire({
        title: 'Vuoi confermare?',
        text: "Clicca sotto",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'CREA TORNEO!'
      }).then((result) => {
        if (result.isConfirmed) {
          createTorneo(titolo, descrizione,tipologia, data, numPosti)
            .then(() => {
              Swal.fire(
                'Torneo creato con successo!'
              );
              navigate('/competizioni');

            }
            )
            .catch(() => {
              Swal.fire(
                'Impossibile effettuare la creazione!'
              );
            });
        }
      });
    }
  }
  
  
return(
  <div>
    <h2>Inserisci i dati del torneo da creare:</h2>
    <Form>
    <Form.Group>
      <Form.Label>Nome Torneo</Form.Label>
      <Form.Control type="text" value={titolo} onChange={(e) => setTitolo(e.target.value)}/>
    </Form.Group>
    <Form.Group>
      <Form.Label>Descrizione</Form.Label>
      <Form.Control type="text" value={descrizione} onChange={(e) => setDescrizione(e.target.value)}/>
    </Form.Group>
    <Form.Group>
      <Form.Label>tipologia</Form.Label>
      <Form.Control type="text" value={tipologia} onChange={(e) => setTipologia(e.target.value)}/>
    </Form.Group>
    <Form.Group>
      <Form.Label>Data</Form.Label>
      <Form.Control type="text" value={data} onChange={(e) => setData(e.target.value)}/>
    </Form.Group>
    <Form.Group>
      <Form.Label>Posti disponibili</Form.Label>
      <Form.Control type="text" value={numPosti} onChange={(e) => setNumPosti(e.target.value)}/>
    </Form.Group>
    <Button variant="primary" onClick={postTorneo}>Inserisci</Button>
    </Form>
  </div>
)

}

export default CreaTorneo