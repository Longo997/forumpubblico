import React, {useEffect, useState} from 'react'
import Torneo from './torneo';
import { Link } from "react-router-dom";
import { getTornei } from '../../database/api/api';
import Button from 'react-bootstrap/Button'
import './tornei.css'

const Competizioni = () => {
  const [tornei, setTornei] = useState([])
  const [role, setRole] = useState("User")



  useEffect(() => {
    getTornei().then(response => setTornei(response.data))
    setRole(sessionStorage.getItem('role'))
  }, []);

  if(role == "Admin"){
    return(
      <div className="competitions-list">
        <hr/>
        <h2>Opzioni organizzatore:</h2>
        <Link to={"/creazione-torneo/"}><Button variant="success">Inserisci Torneo</Button></Link>
        <Link to={"/rimozione-torneo/"}><Button variant="danger">Elimina Torneo</Button></Link>
        <hr/>
        <div className="row">
          {tornei.map(torneo => (
          <Torneo
          id={torneo._id}
          titolo={torneo.titolo}
          data={torneo.data}
          numPosti={torneo.numPosti}
          tipologia={torneo.tipologia}
          descrizione={torneo.descrizione}
          />
          ) )}
        </div>
      </div>
    )
  }
  else {
    return(
      <div className="competitions-list">
        
        <div className="row">
          {tornei.map(torneo => (
          <Torneo
          id={torneo._id}
          titolo={torneo.titolo}
          data={torneo.data}
          numPosti={torneo.numPosti}
          tipologia={torneo.tipologia}
          descrizione={torneo.descrizione}
          />
          ) )}
        </div>
      </div>
    )
  }

    
}
export default Competizioni