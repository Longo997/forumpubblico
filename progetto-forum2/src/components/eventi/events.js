import React, { useEffect, useState } from 'react'
import Card from './card';
import { Link } from "react-router-dom";
import { FaTrashAlt } from "react-icons/fa";
import { FcPlus } from "react-icons/fc";
import Row from 'react-bootstrap/Row'
import { getEventi } from '../../database/api/api.js'

const Events = () => {
  const [posts, setPosts] = useState([])
  const [role, setRole] = useState("User")

  useEffect(() => {
    getEventi().then(response => setPosts(response.data))
    setRole(sessionStorage.getItem('role'))
  }, [])

  if(role == "Admin"){
    return(<div>

      <div class="container">
        <div class="text-box">
          <h1 class="eventi">EVENTI</h1>
        </div>
          
      </div>
        
      
          <div className="event-list">
            <div>
              <h2>Opzioni organizzatore:</h2>
              <Link to={"/creazione-evento/"}><FcPlus size={30} /></Link>
              <Link to={"/rimozione-evento/"}><FaTrashAlt size={30} color="red" /></Link>
              <hr/>
              </div>
              
            <Row>
            {posts.map(card => (
                <Card 
                  id={card.id}
                  title={card.titolo}
                  body={card.descrizione}
                  date={card.data}
                  numPosti={card.numPosti}
                  image={card.imgPath}
                />
              ))}
            </Row>
          </div>
          </div>)
  }
  else{return (
<div>

<div class="container">
	<div class="text-box">
		<h1 class="eventi">EVENTI</h1>
	</div>
		
</div>
	

    <div className="event-list">
    
    
      <Row>
      {posts.map(card => (
          <Card 
            id={card._id}
            title={card.titolo}
            body={card.descrizione}
            date={card.data}
            numPosti={card.numPosti}
            image={card.imgPath}
          />
        ))}
      </Row>
    </div>
    </div>
  )}

  
}
export default Events