import React, {useState} from 'react'
import { useNavigate } from "react-router-dom";
import { createEvento } from '../../database/api/api';
import Button from 'react-bootstrap/Button';
import './eventi.css'
import Swal from 'sweetalert2';
import Form from 'react-bootstrap/Form'

const CreaEvento = () => {
  
  const [titolo, setTitolo] = useState('')
  const [descrizione, setDescrizione] = useState('')
  const [data, setData] = useState('')
  const [numPosti, setNumPosti] = useState('')
  const [image, setImage] = useState('')

  let navigate = useNavigate();

  const postEvento = (e) => {
    e.preventDefault()
    if(titolo==="" || descrizione==="" || data==="" || numPosti==="" || image===""){
      alert("Inserisci tutti i campi richiesti");
    }else{
      Swal.fire({
        title: 'Vuoi confermare?',
        text: "Clicca sotto",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'CREA EVENTO!'
      }).then((result) => {
        if (result.isConfirmed) {
          createEvento(titolo, descrizione, data, numPosti, image)
            .then(() => {
              Swal.fire(
                'Evento creato con successo!'
              );
              navigate('/eventi');

            }
            )
            .catch(() => {
              Swal.fire(
                'Impossibile effettuare la creazione!'
              );
            });
        }
      });
    }
  }

  //accept=".jpg" nell'input

  const provaPost = (e) => {
    createEvento(titolo, descrizione, data, numPosti, image)
    .then(res => console.log(res))
    .catch(err => console.log(err))
  }
  
return(
  <div>
    <div className="form">
  <h2>Inserisci i dati dell'evento da creare</h2>
  <Form>
  <Form.Group>
    <Form.Label>Nome Evento</Form.Label>
    <Form.Control type="text" value={titolo} onChange={(e) => setTitolo(e.target.value)}/>
  </Form.Group>
  <Form.Group>
    <Form.Label>Descrizione</Form.Label>
    <Form.Control type="text" value={descrizione} onChange={(e) => setDescrizione(e.target.value)}/>
  </Form.Group>
  <Form.Group>
    <Form.Label>Data</Form.Label>
    <Form.Control type="text" value={data} onChange={(e) => setData(e.target.value)}/>
  </Form.Group>
  <Form.Group>
    <Form.Label>Numero di posti</Form.Label>
    <Form.Control type="text" value={numPosti} onChange={(e) => setNumPosti(e.target.value)}/>
  </Form.Group>
  <Form.Group>
    <Form.Label htmlFor="file">Carica locandina</Form.Label>
    <input type="file" id="file" accept=".jpg" onChange={event => {
      const file = event.target.files[0]
      setImage(file)
    }}/>
  </Form.Group>
  <Button variant="primary" onClick={provaPost}>Inserisci</Button>
  </Form>
  </div>
  </div>
)
}

export default CreaEvento