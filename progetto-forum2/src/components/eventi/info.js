import React, { useEffect, useState } from 'react'
import {useLocation} from 'react-router-dom'
import Card from 'react-bootstrap/Card'
import { getImage } from '../../database/api/api.js'

const Info = () => {
  const [image, setImage] = useState([]) 

  let location = useLocation();

  useEffect  (() => {
    console.log(location.state)
    getImage(location.state.image).then(response => setImage(response))
  }, [])


  return (
    <div>
      <Card style={{marginRight:'auto',marginLeft:'auto',   textAlign: 'center', padding: '2%', marginTop:'2cm'}}>
          <Card.Body>
            <img src={`${image}`} alt=""  width="500" height="500"/>
            <Card.Title>{location.state.title}</Card.Title>
            <Card.Subtitle>{location.state.date} posti: {location.state.numPosti}</Card.Subtitle>
            <Card.Text>{location.state.body}</Card.Text>
          </Card.Body>
      </Card>
    </div>
  )
}

export default Info