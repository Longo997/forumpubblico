import React, {useEffect, useState} from 'react'
import { useParams } from 'react-router-dom';
import api from '../../database/api/risorse';
import { getEventi } from '../../database/api/api.js'
import { Link, useNavigate } from "react-router-dom";
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

const ConfermaRimozione = () => {
  
    let {id} = useParams();
    let navigate = useNavigate();
  
    const [posts, setPosts] = useState([])

    useEffect(() => {
        getEventi().then(response => setPosts(response.data))
      }, [])

    const doDelete = async () => {
        await api.delete(`/eventi/${id}`);
        alert("evento eliminato");
        navigate('/eventi');
    }

    return (
    <div>
        {posts.filter(ev=> (ev.id==id)).map(evID=>(
            <Card>
            <Card.Body>
            <Card.Title>Confermi cancellazione di: {evID.titolo}?</Card.Title>
            <Button variant="success" onClick={doDelete}>Conferma</Button>
            <Link to={"/rimozione-evento/"}><Button variant="danger">Annulla</Button></Link>
            </Card.Body>
            </Card>
        ))}
    </div>
  )
}

export default ConfermaRimozione